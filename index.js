const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

let firstName = "";
let lastName = "";

txtFirstName.addEventListener('keyup', displayName);
txtLastName.addEventListener('keyup', displayName);

function displayName(event){
	firstName = txtFirstName.value;
	lastName = txtLastName.value;
	spanFullName.innerHTML = firstName + " " +lastName;
}